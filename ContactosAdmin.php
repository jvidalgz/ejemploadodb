<?php

 	include('adodb5/adodb.inc.php');

	$db = NewADOConnection('mysql');
	$db->Connect('localhost','root','toor','contacto');


	//Estamos realizando OUTPUT de código HTML, fijarse en que no es mas que una string que se va concatenando
	$html = '<table border = "1">
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Celular</th>
				<th>Email</th>
				<th>Acciones</th>';

	$query = "SELECT * FROM contacto";

	$data = $db->Execute($query);

	while (!$data->EOF && $data) { //$data->EOF es un atributo de la clase $data que toma true o false, dependiendo si llegamos al final
		$html.='<tr>
			<td>'.$data->fields[1].'</td>
			<td>'.$data->fields[2].'</td>
			<td>'.$data->fields[3].'</td>
			<td>'.$data->fields[4].'</td>
			<td><a href="ContactoFormUpdate.php?id='.$data->fields[0].'"> Editar </a></td> 
		</tr>';
		$data->MoveNext(); //el método MoveNext() mueve el cursor hacia adelante
	}

	$html.='</table>';

?>

<html>
<head>
	<title>Contactos</title>
</head>
<body>
	<h1>Mis Contactos</h1>
	<?php echo $html;?> <!--ACÁ IMPRIMIMOS EL CÓDIGO HTML GENERADO DINÁMICAMENTE ANTERIORMENTE-->
</body>
</html>