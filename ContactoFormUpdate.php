<?php

 	include('adodb5/adodb.inc.php');

	$db = NewADOConnection('mysql');
	$db->Connect('localhost','root','toor','contacto');

	$id = $_GET['id']; //Recuperamos el parámetro de la petición GET que viene en la uri. id=valor

	$datos = $db->Execute("SELECT * FROM contacto WHERE id = '$id'");

?>

<html>
<head>
	<title>Ejemplo ADO DB</title>
</head>
<body>
	<h1>Contactos</h1>
	<h2>Ingrese datos de contacto:</h2>
	<table>
	<form name = "form_contacto" method = "POST" action = "ContactoUpdate.php">
		<tr>
			<td>Nombre:</td>
			<td><input type = "text" name = "nombre" required value = "<?php echo $datos->fields[1]?>"/></td>
		</tr>
		<tr>
			<td>Apellido:</td>
			<td><input type = "text" name = "apellido" required value = "<?php echo $datos->fields[2]?>"/></td>
		</tr>
	    <tr>
			<td>Celular:</td>
			<td><input type = "number" name = "celular" min = "0" required value = "<?php echo $datos->fields[3]?>"/></td>
		</tr>
	    <tr>
			<td>Email:</td>
			<td><input type = "text" name = "email" required value = "<?php echo $datos->fields[4]?>"/></td>
		</tr>
		<input type = "hidden" name = "id" value = "<?php echo $datos->fields[0]?>"/> <!--OJO CON EL INPUT TYPE HIDDEN. ES UN INPUT PERO NO SE VE, SIRVE PARA ENVIAR DATOS POR POST-->
	    <tr>
			<td><input type = "submit" name = "guardar" value = "Actualizar Datos"/></td>
		</tr>
	</form>

	</table>

</body>
</html>