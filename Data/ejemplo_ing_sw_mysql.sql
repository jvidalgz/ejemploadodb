/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     05/08/2014 22:00:01                          */
/*==============================================================*/


drop table if exists contacto;

/*==============================================================*/
/* Table: contacto                                              */
/*==============================================================*/
create table contacto
(
   id                   int not null auto_increment,
   nombre               varchar(30),
   apellido             varchar(70),
   celular              varchar(11),
   email                varchar(50),
   primary key (id)
);

select * from contacto;